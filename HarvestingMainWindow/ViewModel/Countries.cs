﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarvestingMainWindow.ViewModel
{
    public class Countries : List<String>
    {
        public Countries()
        {
            Add("French");
            Add("German");
            Add("Italy");
            Add("Spain");
        }
    }
}
