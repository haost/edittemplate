﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarvestingMainWindow.ViewModel
{
    public class Friends : ObservableCollection<Friend>
    {
        public void Add(ObservableCollection<Friend> list)
        {
            foreach (var t in list)
                this.Add(t);
        }
    }
}
