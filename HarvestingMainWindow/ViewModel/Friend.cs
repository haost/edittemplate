﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarvestingMainWindow.ViewModel
{
    public class Friend
    {
        public String Name { get; set; }
        public String Country { get; set; }
        public Gender Gender { get; set; }

        public Friend(String name, String country, Gender gender)
        {
            Name = name;
            Country = country;
            Gender = gender;
        }
    }
}
