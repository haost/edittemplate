﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using HarvestingMainWindow.ViewModel;

namespace HarvestingMainWindow
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //List<Friend> itemp = new List<Friend>();
        ObservableCollection<Friend> itemp = new ObservableCollection<Friend>();

        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
        }

        //-------------------------------------------------------------------------

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            itemp.Add(new Friend("Albert", "Italy", Gender.Male));
            itemp.Add(new Friend("Albert", "Italy", Gender.Male));
            itemp.Add(new Friend("Albert", "Italy", Gender.Male));
            itemp.Add(new Friend("Albert", "Italy", Gender.Male));
            itemp.Add(new Friend("Lois", "French", Gender.Male));
            itemp.Add(new Friend("Lois", "French", Gender.Male));
            itemp.Add(new Friend("Albert", "Italy", Gender.Male));
            itemp.Add(new Friend("Albert", "Italy", Gender.Male));

            Friends friends = new Friends();
            friends.Add(itemp);
            DataContext = friends;


        }

        //-------------------------------------------------------------------------

        private void DataGrid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {

        }
    }

   
}
